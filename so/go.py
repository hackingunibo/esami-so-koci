import os,sys,requests,re,string,urllib
from lxml import html

def createSnipp(fname,fsnipp):
    valid_chars = "-_.() %s%s" % (string.ascii_letters, string.digits)
    filename = ''.join(c for c in fname if c in valid_chars)
    filename = filename.replace(' ','_') + ".html"
    file = open(filename,"w+")
    file.write(str(fsnipp))
    file.close()



print "START"
def f():
    base_page = "http://so.v2.cs.unibo.it/wiki/index.php?title=Esercizi_di_lettura_di_codice_C"
    page = requests.get(base_page)
    tree = html.fromstring(page.content)
    articles = tree.xpath('//p/a')
    for i,x in enumerate(articles):
        link = x.attrib["href"]
        page = f = urllib.urlopen("http://so.v2.cs.unibo.it/" +link)
        snipp = f.read()
        createSnipp(x.text,snipp)
        print "{} / {}".format(i,len(articles))

f()
print "END"
"""
print "START"

for i in range(2,13):
    base_page = "http://c.happycodings.com/code-snippets/page{}.html".format(str(i))
    page = requests.get(base_page)
    tree = html.fromstring(page.content)
    articles = tree.xpath('//article/div/a')
    for i,x in enumerate(articles):
        link = x.attrib["href"]
        page = requests.get(link)
        tree = html.fromstring(page.content)
        snipp = tree.xpath('//code//text()')
        createSnipp(x.text,snipp)
        print "{} / {}".format(i,len(articles))

print "END"
"""
