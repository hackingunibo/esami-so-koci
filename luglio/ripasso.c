#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

int main(int argc, char const *argv[]) {

  pid_t cp = fork();
  if (cp < 0) {
    printf("Errore durante la fork\n");
    return -1;
  }
  if (cp == 0) {
    printf("Sono il figlio\n");
    // figlio
  } else {
    // padre
    printf("Sono il padre\n");
  }
  return 0;
}
