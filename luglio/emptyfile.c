#define _GNU_SOURCE
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

void seek_write(const char *data, int wsize, int seek, int dsize) {
  int fd = open("data", O_RDWR);
  // Now seek_write
  if (lseek(fd, seek, SEEK_SET) != seek)
    printf("Errore");
  // Now write in requested blocks..
  for (int c = dsize / wsize; c--;)
    if (write(fd, data, wsize) != wsize)
      printf("Errore");
  close(fd);
}

int main(int argc, char const *argv[]) {

  char *data = mmap(0, 512, PROT_READ, MAP_ANON | MAP_PRIVATE, 0, 0);
  printf("%s\n", data);
  FILE *fd = fopen("data", "wb");
  fseek(fd, (2 * 1024 * 1024) - 1, SEEK_SET);
  fputc(0, fd);
  fclose(fd);

  printf("Starting write...\n");
  int dimensione = 2 * 1024 * 1024; // MB
  for (int i = 0; i < 10; ++i)
    seek_write(data, 512, 0, dimensione);

  return 0;
}
