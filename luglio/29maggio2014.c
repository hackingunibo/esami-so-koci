/*
Scrivere un programma con un solo parametro.
Come prima cosa il programma deve creare una directory con il path specificato
nel parametro. Se la directory esiste gia' o si
verifica un errore nella creazione, il programma deve terminare. Chiameremo
questa directory “directory-base”
Il programma usando inotify rimane in attesa e stampa una riga di log per ogni
file o directory creato o cancellato nella
directory-base. (solo nella directory-base, non nelle sottodirectory).
Quando viene cancellata la directory-base il programma termina.
*/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/inotify.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#define EVENT_SIZE (sizeof(struct inotify_event))
#define BUF_LEN (1024 * (EVENT_SIZE + 16))

int main(int argc, const char *argv[]) {
  int ifd, wd; // inotfiy descriptor, inotify watch descriptor
  char buffer[BUF_LEN];
  int len;
  int i = 0;
  if (argc != 2) {
    printf("manca il parametro");
    return (-1);
  }
  const char *directorybase = argv[1];
  if (mkdir(directorybase, 0700) == -1) {
    printf("errore durante la creazione della directory");
    return (-1);
  }
  ifd = inotify_init();
  if (ifd == -1) {
    printf("errore durante inotify_init()");
    return (-1);
  }
  wd = inotify_add_watch(ifd, directorybase, IN_MODIFY | IN_CREATE | IN_DELETE);
  if (wd == -1) {
    printf("errore durante watch()");
    return (-1);
  }
  //   Reading events from an inotify file descriptor
  // eturns a buffer containing struct inotify_event

  len = read(ifd, buffer, BUF_LEN);
  if (len < 0) {
    printf("errore durante read()");
    return (-1);
  }
  while (1) {

    while (i < len) {
      printf("OK");
      struct inotify_event *evento = (struct inotify_event *)&buffer[i];
      if (evento->len) {
        if (evento->mask & IN_CREATE) {
          printf("The file %s was created.\n", evento->name);
        } else if (evento->mask & IN_DELETE) {
          printf("The file %s was deleted.\n", evento->name);
        } else if (evento->mask & IN_MODIFY) {
          printf("The file %s was modified.\n", evento->name);
        }
      }
      i += EVENT_SIZE + evento->len;
    }
  }
  (void)inotify_rm_watch(ifd, wd);
  (void)close(ifd);

  return 0;
}
