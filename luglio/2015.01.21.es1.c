/*
Scrivere un programma C di nome filepipe che abbia come unico parametro il
pathnae di un file di testo.
Questo file contiene due comandi con I rispettivi parametri, uno per riga.
Il programma deve mettere in esecuzione concorrente
I due comandi in modo che l'output del primo venga fornito
come input del secondo usando una pipe.
Il programma deve terminare quando entrambi I comandi sono terminati.
Esempio: se il file ffff contiene:
ls -l
tac
il comando:
filepipe ffff
deve restituire lo stesso output del comando:
ls -l | tac
*/
#include <s2argv.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
  if (argc != 2) {
    fprintf(stderr, "Expected 1 parameter");
    return (EXIT_FAILURE);
  }
  // read file
  FILE *stream;
  char *command1, *command2 = NULL;
  size_t len = 0;
  ssize_t read;
  const char *pathname = argv[1];
  // pipe
  int fd[2]; // fd[0]reading, fd[1] writeing

  stream = fopen(pathname, "r");
  if (stream == NULL)
    exit(EXIT_FAILURE);

  // leggo primo comando

  if (read = getline(&command1, &len, stream) != -1) {
    //  printf("Retrieved line %d of length %zu :\n", i, read);
    printf("%s", command1);
  } else {
    printf("Errore durante la lettura del file\n");
    return (EXIT_FAILURE);
  }

  // leggo secondo comando

  if (read = getline(&command2, &len, stream) != -1) {
    //  printf("Retrieved line %d of length %zu :\n", i, read);
    printf("%s", command2);
  } else {
    printf("Errore durante la lettura del file\n");
    return (EXIT_FAILURE);
  }

  pid_t cp = fork();
  if (cp < 0) {
    printf("Errore durante  fork\n");
    return (EXIT_FAILURE);
  }
  if (pipe(fd) == -1) {
    printf("Errore durante  pipe\n");
    return (EXIT_FAILURE);
  }

  if (cp == 0) {
    printf("Sono il figlio\n");
    close(fd[1]);
    dup2(fd[0], STDOUT_FILENO); // redirect output to stdout
    execsp(command1);
    return (EXIT_FAILURE);

  } else {
    printf("Sono il padre\n");
    close(fd[0]);
    dup2(fd[1], STDIN_FILENO);
    wait(NULL);
    execsp(command2);
    return (EXIT_FAILURE);
  }

  free(command1);
  free(command2);

  fclose(stream);
  exit(EXIT_SUCCESS);
}
