#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
  sleep(1);
  printf("Hello world! \n");
  sleep(1);

  if (strcmp(argv[1], "1") == 0)
    printf("Hello world1!\n");
  if (strcmp(argv[1], "2") == 0)
    printf("Hello world2!\n");
  exit(EXIT_FAILURE);
}
