/*
) Scrivere un programma “riattiva” che lancia un comando
con i propri parametri.
Riattiva ssh piripicchio.domain.it
Se il comando termina correttamente (i.e. Con exit status 0)
 l'esecuzione termina, altrimenti il comando viene
rieseguito. Tutte le volte che il comando termina con exit status
diverso da zero deve essere riattivato.
*/

#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/types.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <unistd.h>

int main(int argc, char *argv[]) {

  char **args = argv + 1;
  //  printf("%s\n", argv[1]);
  execv(argv[1], argv);
  return 0;
  int pid = fork();

  if (pid == 0) {
    // child process
    printf("ciao io sono il figlio è questo è il mio pid %d\n", getpid());

    execv("./hello", &argv[1]);
    printf("EXECV Failed from child\n");
  } else if (pid > 0) {
    printf("ciao io sono il padre è questo è il mio pid %d\n", getpid());
    // parent process
    int status;
    waitpid(pid, &status, 0);

    while (status != 0) {

      int csp = fork(); // restart
      waitpid(csp, &status, 0);
    }
    printf("Programma terminato correttamente\n");
    return 0;

  } else {
    printf("forkFailed\n");
  }
  return 0;
}
